class NumberDigits(val number: Int) {

    companion object {
        fun Int.toNumberDigits(): NumberDigits{
            return NumberDigits(this)
        }
    }

    fun getCountDigits(): Int = number.toString().length

    fun getDigit(index: Int): Int = number.toString()[index - 1].digitToInt()

    fun getStringNumber(): String = number.toString()

    fun getCharDigit(index: Int): Char = number.toString()[index - 1]
}