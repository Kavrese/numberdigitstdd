import NumberDigits.Companion.toNumberDigits
import org.junit.jupiter.api.Test
import java.lang.IndexOutOfBoundsException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class NumberDigitsTest {
    @Test
    fun testCountDigits(){
        assertEquals(1.toNumberDigits().getCountDigits(), 1)
        assertEquals(12.toNumberDigits().getCountDigits(), 2)
        assertEquals(11.toNumberDigits().getCountDigits(), 2)
        assertEquals(0.toNumberDigits().getCountDigits(), 1)
        assertEquals(123.toNumberDigits().getCountDigits(), 3)
        assertEquals(123456789.toNumberDigits().getCountDigits(), 9)
        assertEquals(987654321.toNumberDigits().getCountDigits(), 9)
        assertEquals(1234567890.toNumberDigits().getCountDigits(), 10)
        assertEquals(876543210.toNumberDigits().getCountDigits(), 9)
    }

    @Test
    fun testGetDigit(){
        assertEquals(1.toNumberDigits().getDigit(1), 1)
        assertEquals(0.toNumberDigits().getDigit(1), 0)
        assertEquals(12.toNumberDigits().getDigit(1), 1)
        assertEquals(12.toNumberDigits().getDigit(2), 2)
        assertEquals(541.toNumberDigits().getDigit(1), 5)
        assertEquals(541.toNumberDigits().getDigit(2), 4)
        assertEquals(541.toNumberDigits().getDigit(3), 1)
        assertEquals(9999.toNumberDigits().getDigit(3), 9)
        assertEquals(9999.toNumberDigits().getDigit(2), 9)
        assertFailsWith<IndexOutOfBoundsException>{
            123.toNumberDigits().getDigit(0)
            0.toNumberDigits().getDigit(0)
            1.toNumberDigits().getDigit(0)
            1.toNumberDigits().getDigit(2)
            12.toNumberDigits().getDigit(3)
            13.toNumberDigits().getDigit(4)
        }
    }

    @Test
    fun testGetStringNumber(){
        assertEquals(1.toNumberDigits().getStringNumber(), "1")
        assertEquals(12.toNumberDigits().getStringNumber(), "12")
        assertEquals(11.toNumberDigits().getStringNumber(), "11")
        assertEquals(0.toNumberDigits().getStringNumber(), "0")
        assertEquals(123.toNumberDigits().getStringNumber(), "123")
        assertEquals(123456789.toNumberDigits().getStringNumber(), "123456789")
        assertEquals(987654321.toNumberDigits().getStringNumber(), "987654321")
        assertEquals(1234567890.toNumberDigits().getStringNumber(), "1234567890")
        assertEquals(876543210.toNumberDigits().getStringNumber(), "876543210")
    }

    @Test
    fun testGetCharDigit(){
        assertEquals(1.toNumberDigits().getCharDigit(1), '1')
        assertEquals(0.toNumberDigits().getCharDigit(1), '0')
        assertEquals(12.toNumberDigits().getCharDigit(1), '1')
        assertEquals(12.toNumberDigits().getCharDigit(2), '2')
        assertEquals(541.toNumberDigits().getCharDigit(1), '5')
        assertEquals(541.toNumberDigits().getCharDigit(2), '4')
        assertEquals(541.toNumberDigits().getCharDigit(3), '1')
        assertEquals(9999.toNumberDigits().getCharDigit(3), '9')
        assertEquals(9999.toNumberDigits().getCharDigit(2), '9')
        assertFailsWith<IndexOutOfBoundsException>{
            123.toNumberDigits().getCharDigit(0)
            0.toNumberDigits().getCharDigit(0)
            1.toNumberDigits().getCharDigit(0)
            1.toNumberDigits().getDigit(2)
            12.toNumberDigits().getDigit(3)
            13.toNumberDigits().getDigit(4)
        }
    }
}